const express = require('express');
const expresslayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');


const app = express();

//passport config
require('./config/passport')(passport);

//db connection
const db = require('./config/keys').MongoURI;

//connect to mongodb
mongoose.connect(db, { useNewUrlParser: true })
.then(() => console.log('Mongo db connected...'))
.catch(err => console.log(err));


//EJS
app.use(expresslayouts);
app.set('view engine','ejs');

//Body parser
app.use(express.urlencoded({ extended: false })); 

//express session
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));

//passport middleware
app.use(passport.initialize());
app.use(passport.session());

//connect flash
app.use(flash());

//Global vars
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});


//Routes
app.use('/', require('./routs/index'));
app.use('/users',require('./routs/users'));


app.listen(3000,()=>
{
  console.log('server is working');
})