
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const passport = require('passport');

//Use Model
const User = require('../models/User');

//Login Page
router.get('/login',(req,res) => res.render('login'));

//Register Page
router.get('/register',(req,res) => res.render('register'));

//Register Handle
router.post('/register',(req,res) => {
    const { name, email, password, password2 } = req.body;
    let errors = [];

      //Check required fields
      if(!name || !email || !password || !password2) {
        errors.push({msg: 'please fill in all fields'});
    }

    //Check password match
    if(password !== password2) {
        errors.push({msg: 'passwords do not match'});
    }
    //Check password length
    if(password.length < 6 ) {
        errors.push({msg: 'password must atleast 6 characters'});
    }

    if(errors.length > 0) {
        res.render('register',{
            errors,
            name,
            email,
            password,
            password2
        });
    } else {
       //validation passed
       User.findOne({ email: email })
       .then (user => {
           if(user){
               //User Exist
               errors.push({ msg: 'Email already Registerd' });
            res.render('register',{
                errors,
                name,
                email,
                password,
                password2
            });
           }   else {
                    const newUser = new User({
                        name,
                        email,
                        password
                    });

                    //Hash password
                    bcrypt.genSalt(10, (err,salt) => 
                     bcrypt.hash(newUser.password,salt, (err,hash) => {
                         if(err) throw err;
                         //set password to hash
                         newUser.password = hash;
                         //save user
                         newUser.save()
                          .then(user => {
                              req.flash('success_msg','You are succesfully Registered.');
                              res.redirect('/users/login');
                          })
                          .catch(err => console.log(err));
                     }))

                    // console.log(newUser)
                    // newUser.save();
                    // res.send('hello');

                }
                 });
    }
});

//login handle
router.post('/login', (req, res, next) => {
    passport.authenticate('local',{
        successRedirect: '/dashboard',
        failureRedirect: '/users/login',
        failureFlash: true
    })(req, res, next);
});

//logout handle
router.get('/logout', (req, res,) => {
    req.logout();
    req.flash('success_msg','You are logged out');
    res.redirect('/users/login'); 
});

module.exports = router;