const mongoose = require('mongoose');
const option = {
    socketTimeoutMS: 30000,
    keepAlive: true,
    reconnectTries: 30000
};


mongoose.connect('mongodb+srv://scott:tiger@tmcluster-hxkuu.mongodb.net/test?retryWrites=true&w=majority', option).then(function(){
    //connected successfully
    console.log('Successfully connected to database');
}, function(err) {
    //err handle
    console.log('Not connected to database ' + err);
});