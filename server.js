const express = require('express');
const connectdb = require('./db/connectdb');
const app = express();
const Port = process.env.Port || 3001;

app.listen(Port,()=> console.log('Server started.'))